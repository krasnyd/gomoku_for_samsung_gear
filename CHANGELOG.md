### v0.0.4
* Changed zooming mechanism
* Changed function of back button
* Added dialog after game ends

### v0.0.3
* Fixed an algorithm that determines who won.

### v0.0.2
* Graphical improvements

### v0.0.1
* Basic functional program