var savedGames;

window.addEventListener("load", function(event) {
	
	function loadSaves(){
		savedGames = savedGames.split(/\r?\n/);
		for (var i = 0; i < savedGames.length; i++){
			if(savedGames[i] === ""){
				continue;
			}
			var splitedGame = savedGames[i].split(";");
			
			var game = splitedGame[0];
			var link = splitedGame[1];
			console.log("Loading game: " + link);
			var onClickEvent = "goTo(\""+link+"\")";
  
			var date = new Date(parseInt(game));
			
			$("#link-menu").append(
				"<li class='li-has-action-icon'>"+
				"<div class='li-action-text' style='font-size: 30px;text-align:left' onclick='"+onClickEvent+"'>"+formatTime(date)+"<br/>"+formatDate(date)+" - "+opponent(link)+"</div>"+
				"<button class='li-action-icon-button li-action-delete' onclick='deleteGame(\""+game+"\")'></button>"+
				"</li>"
			);
		}
		
		// create and refresh ListView
		var element = document.getElementById("link-menu"),
		listv = tau.widget.Listview(element);
	    listv.refresh();
	}

	tizen.filesystem.resolve('documents/GomokuForSamsungGear/games.txt', 
		function(file){
			file.readAsText(
				function(contents) {
					savedGames = contents;
					loadSaves();
				}.bind(file), 
				printError, 
				"UTF-8"
			);
		}, 
		printError
	);
	
}, false);

function deleteGame(id){
	goTo("delete-game.html?id="+id);
}

function formatTime(date) {
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();
	
    minutes = digitsSet(minutes);
    seconds = digitsSet(seconds);
   
    return hours + ":" + minutes + ":" + seconds;
}

function formatDate(date) {
	var day = date.getDate();
	var month = date.getMonth()+1;
   
    return day + ". " + month + "." ;
}

function opponent(string){
	if(string.includes("robot")){
		return "PvR";
	} else if(string.includes("player")){
		return "PvP";
	} else {
		console.error("missing opponent");
	}
}