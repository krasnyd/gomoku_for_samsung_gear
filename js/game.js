//http://stefangabos.github.io/Zebra_Gomoku/
var board = new Array(225);
var BOARD_SIZE = 15;
var GAMENAME;
var ENEMY; // robot or player

var GAME_PAUSED = false;

var numberOfClicks = 0;
var firstClick;
var secondClick;
(function () {
	window.addEventListener('tizenhwkey', function(ev) {
		if (ev.keyName === "back") {
			numberOfClicks++;
			console.log("Clicks: " +numberOfClicks);
			if(numberOfClicks == 1){
				setTimeout(function(){
				    console.log("Konec Eventu: " + numberOfClicks);
				    console.log(GAME_PAUSED);
				    if(GAME_PAUSED || numberOfClicks > 1){
				    	goTo("index.html");
				    } else {
				    	confirmPlay();
				    }
				    numberOfClicks = 0;
				}, 500);
			}
		}
	});
}());

window.addEventListener("load", function(event) {

    // Generate board
    var boardIterator = 0;
    for (var i = 0; i < BOARD_SIZE; i++) {
        $("#content-table").append("<tr id='row"+i+"'></tr>");
        for(var b = 0; b < BOARD_SIZE; b++){
            $("#row"+i).append("<td id='cell-"+boardIterator+"' onclick='clickOnCell(this.id)'></td>");
            board[boardIterator] = 0;
            boardIterator++;
        }
    }

    // TODO Compuer will play to the middle
    if(CURRENT_PLAYER == COMPUTER_ID){
        playAt("#cell-112", COMPUTER_ID);
    }

    // Highlight center
    $("#cell-112").addClass("startingPosition");

    // Zooming
    var ZOOM = 100;
    var SCROLL_STEP = 25;
    
    document.addEventListener('rotarydetent', function (ev) {
        var direction = ev.detail.direction;
        var newZoom = ZOOM;
        
        if (direction === 'CW') {
            /* Right direction */
        	newZoom += SCROLL_STEP;
        } else if (direction === 'CCW') {
            /* Left direction */
        	newZoom -= SCROLL_STEP;
        }
        
        if (newZoom >= 100 && newZoom <= 400){
	        ZOOM = newZoom;
        	
	        console.log("Rotace" + ZOOM);
        	$("#content").animate({zoom: ZOOM + "%"}, 200);
        }
    });
    
    
    // Robot or player
    var oponentName = findGetParameter("opponent");
    if(oponentName == "robot"){
    	ENEMY = oponentName;
    } else if (oponentName == "player"){
    	ENEMY = oponentName;
    } else {
    	console.error("missing oponent name");
    }
    
    // Loading game
    var getName = findGetParameter("gameName");
    if (getName == null){ // new game
    	var date = new Date().getTime();
    	GAMENAME = date;
    	saveGame();
    } else { // old game -> load it
    	GAMENAME = getName;
    	loadGame();
    }
},false);

function loadGame(){
	tizen.filesystem.resolve('documents/GomokuForSamsungGear/'+GAMENAME+'.txt', 
		function(file){
			file.readAsText(
				function(contents) {
					playBySave(contents);
				}.bind(file), 
				printError, 
				"UTF-8"
			);
		}, 
		printError
	);
}

function getFULLNAME(){
	var text = "";
	text += "game.html?gameName="+GAMENAME+"&opponent="+ENEMY;
	return text;
}

function playBySave(textFromSave){
	var moves = textFromSave.split(/\r?\n/);
	for (var i = 0; i < moves.length; i++){
		if(moves[i] === ""){
			continue;
		}
		var splitedGame = moves[i].split(";");
		
		var move = splitedGame[0];
		var movedBy = splitedGame[1];
		console.log("Player: "+movedBy + " move to "+move);
		
		GAME_PAUSED = true;
		playAt(move, movedBy);
		GAME_PAUSED = false;
	}
} 

function saveGame(){
	// add game to list
	tizen.filesystem.resolve("documents/GomokuForSamsungGear/games.txt",
		function(file){
			file.openStream('a',
				function(stream) {
					var text = GAMENAME + ";" + getFULLNAME();
					console.log("Adding to games.txt: "+text);
					stream.write("\n"+text);
				}.bind(file),
				printError.bind(file)
			);
		}, 
		printError
	);
	
	// create empty file for moves
	tizen.filesystem.resolve("documents/GomokuForSamsungGear/",
		function(dir){
			try {
				console.log("Creating file: "+GAMENAME+".txt");
				dir.createFile(GAMENAME+".txt");
			} catch (e) {
				printError(e);
			}
		}, 
		printError
	);
}

var PLAYER_ID = 1; // first player
var PLAYER_ID2 = 2;

var COMPUTER_ID = PLAYER_ID2;

var CURRENT_PLAYER = PLAYER_ID; // TODO kdo začíná

var currentId = null;

function clickOnCell(clickedId){
    if(GAME_PAUSED){
    	return;
    }
    
	
    var clickedClass = $("#" + clickedId);
    if(!clickedClass.hasClass("player1") && !clickedClass.hasClass("player2") && !clickedClass.hasClass("selected")){
        
            //playAt(clickedId, CURRENT_PLAYER); // zahraj na id člověk
        
	    $(clickedClass).addClass("selected");
	    $("#"+currentId).removeClass("selected");
	    currentId = clickedId;
    }
}

function confirmPlay(){
	if(currentId != null){
		playAt(currentId, CURRENT_PLAYER);
		currentId = null;
	}
}

function playAt(clickedId, player){
    console.log("Playing at: "+clickedId + "; "+player);
    
    var num = clickedId.split('-')[1];
    num = parseInt(num);
    
    $("#" + clickedId).removeClass("selected");
    $("#cell-112").removeClass("startingPosition");
    if(player == PLAYER_ID){
        $("#" + clickedId).addClass("player1");
        board[num] = PLAYER_ID;
        //console.log(board);
        CURRENT_PLAYER = PLAYER_ID2;
    } else if(player == PLAYER_ID2){
        $("#" + clickedId).addClass("player2");
        board[num] = PLAYER_ID2;
        //console.log(board);
        CURRENT_PLAYER = PLAYER_ID;
    } else {
        console.error("Error while playAt()");
    }
    
    highlightLast(clickedId);
    
    if(GAME_PAUSED == false){
	    setTimeout(function(){
	        saveMove(clickedId, player);
	    },500);

	    checkBoard(num, player);
    }
    
    if(CURRENT_PLAYER == COMPUTER_ID && GAME_PAUSED == false && ENEMY == "robot"){
        computer_move();
    }
}

var lastHighligh="";
function highlightLast(id){
	$("#" + id).addClass("highlighted");
	if(lastHighligh != ""){
		$("#" + lastHighligh).removeClass("highlighted");
	}
	lastHighligh = id;
}

function saveMove(clickedId, player){
	tizen.filesystem.resolve("documents/GomokuForSamsungGear/"+GAMENAME+".txt",
		function(file){
			file.openStream('a',
				function(stream) {
					var text = clickedId + ";" + player;
					console.log("Adding to "+GAMENAME+".txt: "+text);
					stream.write("\n"+text);
				}.bind(file),
				printError.bind(file)
			);
		}, 
		function(){console.warn("Play cannot be added to file with game");}
	);
}

function checkBoard(newPosition, player){
	console.log("Checking board from position: " + newPosition +" by player "+player);

	var smery = [
		1, // left / right
		14, // right-top / left-bottom
		15, // top / bottom
		16 // left-top / right-bottom
	];
	for(var i=0; i < smery.length; i++){
		var hasLeft = true;
		var hasRight = true;
		var left = newPosition;
		var right = newPosition;

		var veSmeru = 1;
		var lastItems = [];
		for(var b = 0; b <= 4; b++){
			left -= smery[i];
			if (hasLeft){
				if(board[left] == player){
					veSmeru++;
					lastItems.push(left);
				} else {
					hasLeft = false;
				}
			}

			right += smery[i];
			if (hasRight){
				if(board[right] == player){
					veSmeru++;
					lastItems.push(right);
				} else {
					hasRight = false;
				}
			}
		}

		if(veSmeru >= 5){
			console.log(lastItems + " " + lastItems.length);
			for(var r=0; r < lastItems.length; r++){
				console.log("#cell-"+lastItems[r]);
				$("#cell-"+lastItems[r]).addClass("winner");
			}
			$("#cell-"+newPosition).removeClass("highlighted");
			$("#cell-"+newPosition).addClass("winner");
			$("#dialog").css("display", "block");
			
			if(player == 1){
				$("#dialog").css("color", "red");
				$("#dialog").html("Vyhrál hráč <span class='krizek'></span>");
			}
			if(player == 2){
				$("#dialog").css("color", "blue");
				$("#dialog").html("Vyhrál hráč <span class='kolecko'></span>");
			}
			GAME_PAUSED = true;
			console.log("KONEC");
			
			tizen.filesystem.resolve('documents/GomokuForSamsungGear/games.txt', 
				function(file){
					file.readAsText(
						function(contents) {
							loadSaves(contents);
						}.bind(file), 
						printError, 
						"UTF-8"
					);
				}, 
				printError
			);
			break;
		}
	}
}

var outputer;

function loadSaves(savedGames){
	savedGames = savedGames.split(/\r?\n/);
	for (var i = 0; i < savedGames.length; i++){
		if(savedGames[i].includes(GAMENAME)){
			savedGames.splice(i, 1);
			break;
		}
	}
	var output="";
	for (var t = 0; t < savedGames.length; t++){
		output += "\n" + savedGames[t];
	}
	outputer = output;
	rewriteGames();
}

function rewriteGames(){
	tizen.filesystem.resolve("documents/GomokuForSamsungGear/games.txt",
		function(file){
			file.openStream('w',
				function(stream) {
					stream.write(outputer);
					deleteFileGame();
				}.bind(file),
				printError.bind(file)
			);
		}, 
		printError
	);
}

var documentsDir;
function deleteFileGame(){
	tizen.filesystem.resolve("documents/GomokuForSamsungGear",
		function(dir){
			try {
				documentsDir = dir;
				dir.listFiles(del,printError);
			} catch (e) {
				printError(e);
			}
		}, 
		printError
	);
}

function del(files) {
	for (var i = 0; i < files.length; i++) {
		if (files[i].fullPath.includes(GAMENAME)) {
			documentsDir.deleteFile(
				files[i].fullPath,
				function() {
					console.log("File Deleted");
				}, printError
			);
		}
	}
}
