window.addEventListener("load", function(event) {
	var id = findGetParameter("id");
	var outputer;
	
	function loadSaves(savedGames){
		savedGames = savedGames.split(/\r?\n/);
		for (var i = 0; i < savedGames.length; i++){
			if(savedGames[i].includes(id)){
				savedGames.splice(i, 1);
				break;
			}
		}
		var output="";
		for (var t = 0; t < savedGames.length; t++){
			output += "\n" + savedGames[t];
		}
		outputer = output;
		rewriteGames();
	}
	
	function rewriteGames(){
		tizen.filesystem.resolve("documents/GomokuForSamsungGear/games.txt",
			function(file){
				file.openStream('w',
					function(stream) {
						stream.write(outputer);
						deleteFileGame();
					}.bind(file),
					printError.bind(file)
				);
			}, 
			printError
		);
	}
	
	var documentsDir;
	function deleteFileGame(){
		tizen.filesystem.resolve("documents/GomokuForSamsungGear",
			function(dir){
				try {
					documentsDir = dir;
					dir.listFiles(del,printError);
				} catch (e) {
					printError(e);
				}
			}, 
			printError
		);
	}
	
	function del(files) {
		for (var i = 0; i < files.length; i++) {
			if (files[i].fullPath.includes(id)) {
				documentsDir.deleteFile(
					files[i].fullPath,
					function() {
						console.log("File Deleted");
						window.location.href = "saved-games.html";
					}, printError
				);
			}
		}
	}
	
	tizen.filesystem.resolve('documents/GomokuForSamsungGear/games.txt', 
		function(file){
			file.readAsText(
				function(contents) {
					loadSaves(contents);
				}.bind(file), 
				printError, 
				"UTF-8"
			);
		}, 
		printError
	);
},false);