/**
 * Shortcut for window.location.href
 * @param url
 */
function goTo(url){
	window.location.href = url;
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

function printError(error){
	console.error("Error '"+error.name+"': "+error.message);
}

/**
 * Make from one digit number two digit number. 1 -> 01; 21 -> 21
 * @param {int} variable 
 * @returns {String} two digit number
 */
function digitsSet(variable){
	if (variable.toString().length < 2){
		variable = '0' + variable;
	}
	
	return variable;
}