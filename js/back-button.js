(function () {
	window.addEventListener('tizenhwkey', function(ev) {
		if (ev.keyName === "back") {
			var page = document.getElementsByClassName('ui-page-active')[0],
			pageid = page ? page.id : "";
			console.log("Back key pressed: " + pageid);

			if (pageid === "main") {
				try	{
					tizen.application.getCurrentApplication().exit();
				} catch (ignore) {
					console.error("Cannot exit application");
				}
			} else {
				var backPage = $("body").attr("back-page");
				console.log("back" + backPage);
				if (backPage != "" && backPage != undefined){
					goTo(backPage);
				} else {
					window.history.back();
				}
			}
		}
	});
}());