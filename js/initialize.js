window.addEventListener("load",function(event) {
	// Create directory GomokuForSamsungGear
	tizen.filesystem.resolve('documents', 
		function(dir){
			try {
				dir.createDirectory("GomokuForSamsungGear");
			} catch(e){
				console.log("Directory GomokuForSamsungGear already exists. ");
			}
			createGamesFile();
		}, 
		printError
	);
	
	// Create games.txt
	function createGamesFile(){
		tizen.filesystem.resolve('documents/GomokuForSamsungGear', 
			function(dir){
				try {
					dir.createFile("games.txt");
				} catch(e){
					console.log("File games.txt already exists. ");
				}
			}, 
			printError
		);
	}
	
},false);